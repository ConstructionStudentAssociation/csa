// @flow
/* eslint-disable import/no-extraneous-dependencies */
import * as React from "react"
import { ScrollView, Text, StatusBar, Platform } from "react-native"
import { Ionicons } from "@expo/vector-icons"
import { Container } from "../components/container"
import { ListItem, Separator } from "../components/list"
import { AltButton } from "../components/button"

const ICON_PREFIX = Platform.OS === "ios" ? "ios" : "md"
const ICON_COLOR = "#868686"
const ICON_SIZE = 23

export default class Options extends React.PureComponent {
    handleFollowTwitterPress = () => {
        console.log("Press Twitter")
    }

    handleFollowFacebookPress = () => {
        console.log("Press Facebook")
    }

    handleNotificationsPress = () => {
        console.log("Press Notifications")
    }
    handleRateAppPress = () => {
        console.log("Press Rate App")
    }
    handleAboutPress = () => {
        console.log("Press About")
    }
    handleHelpPress = () => {
        console.log("Press Help")
    }
    handleLogoutPress = () => {
        console.log("Logout Pressed")
    }

    render(): React.Node {
        return (
            <Container style={styles.container}>
                <ScrollView>
                    <StatusBar translucent={false} barStyle="default" />
                    <Text>USEFUL STUFF</Text>
                    <ListItem
                        text="Rate the App ❤️"
                        onPress={this.handleRateAppPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <ListItem
                        text="Notifications"
                        onPress={this.handleNotificationsPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <ListItem text="Swap Message Colors" />
                    <Separator />
                    <ListItem text="Hide Phone Number" />
                    <Separator />
                    <Text>STAY IN TOUCH</Text>
                    <ListItem
                        text="Follow on Twitter"
                        onPress={this.handleFollowTwitterPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <ListItem
                        text="Follow on Facebook"
                        onPress={this.handleFollowFacebookPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <Text>SUPPORT</Text>
                    <ListItem
                        text="About"
                        onPress={this.handleAboutPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <ListItem
                        text="Help"
                        onPress={this.handleHelpPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <AltButton
                        subTitle="Sign Up"
                        onPress={this.handleLogoutPress}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                </ScrollView>
            </Container>
        )
    }
}

const styles = {
    container: {}
}
