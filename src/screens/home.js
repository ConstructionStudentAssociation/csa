// @flow
import * as React from "react"
import { View, Text } from "react-native"
import { Container } from "../components/container"

export default class Home extends React.Component {
    render(): React.Node {
        return (
            <Container style={styles.container}>
                <View>
                    <Text> Home Screen </Text>
                </View>
            </Container>
        )
    }
}

const styles = {
    container: {
        justifyContent: "center",
        alignItems: "center"
    }
}
