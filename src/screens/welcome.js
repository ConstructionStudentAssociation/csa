// @flow
import * as React from "react"
import { Text, StatusBar, View } from "react-native"
import { AuthContainer } from "../components/container"
import { DefaultButton, AltButton } from "../components/button"
import { StyleGuide } from "../config/theme"

import Logo from "../components/logo/logo"

export default class Welcome extends React.Component {
    onButtonPress() {
        console.log("Get Started Btn Pressed")
    }

    render(): React.Node {
        return (
            <AuthContainer style={styles.container}>
                <StatusBar barStyle="light-content" translucent={false} />
                <Logo style={styles.logo} />

                <View style={styles.statementContainer}>
                    <Text style={styles.statementHeader}>Our Vision</Text>
                    <Text style={styles.statementBody}>
                        To create a link between the professional and educational worlds
                    </Text>
                </View>

                <View style={styles.statementContainer}>
                    <Text style={styles.statementHeader}>Our Mission</Text>

                    <Text style={styles.statementBody}>
                        To establish and preserve relationships within the industry and to facilitate learning
                        opportunities outside of the classroom, while maintaining a charitable relationship with the
                        surrounding community.
                    </Text>
                </View>

                <DefaultButton title="Get Started" onPress={this.onButtonPress} />

                <View style={styles.altButtonContainer}>
                    <AltButton
                        title="Already have an account?"
                        subTitle="Sign In"
                        onPress={this.onNeedAccountPressed}
                    />
                </View>

                <View style={styles.footerContainer}>
                    <Text style={styles.footerTxt}>
                        <Text>Copyright 2017 Construction Student Association. </Text>
                        <Text>All rights reserved.</Text>
                    </Text>
                </View>
            </AuthContainer>
        )
    }
}

const styles = {
    container: {
        justifyContent: "space-around",
        alignItems: "center"
    },
    logo: {
        resizeMode: "contain",
        top: 10
    },
    statementContainer: {
        flexDirection: "column",
        alignItems: "center"
    },
    statementHeader: {
        color: StyleGuide.palette.warning,
        ...StyleGuide.typography.title2Bold
    },
    statementBody: {
        color: StyleGuide.palette.white,
        ...StyleGuide.typography.callout,
        width: 280,
        top: 8,
        textAlign: "center"
    },
    footerContainer: {
        flexDirection: "row",
        bottom: 28
    },
    footerTxt: {
        ...StyleGuide.typography.caption,
        color: StyleGuide.palette.white
    },
    altButtonContainer: {
        bottom: 30
    }
}
