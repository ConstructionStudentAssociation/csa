// @flow
/* eslint-disable import/no-extraneous-dependencies */
import * as React from "react"
import { ScrollView, Platform, StatusBar } from "react-native"
import { Ionicons } from "@expo/vector-icons"
import { Container } from "../components/container"
import { ListItem, Separator } from "../components/list"

const ICON_PREFIX = Platform.OS === "ios" ? "ios" : "md"
const ICON_COLOR = "#868686"
const ICON_SIZE = 23

export default class Options extends React.PureComponent {
    handleTermsandConditions = () => {
        console.log("Press Terms and Conditions")
    }

    handlePrivatePolicy = () => {
        console.log("Press Private Policy")
    }

    handleCredits = () => {
        console.log("Press Credits")
    }
    handleRateAppPress = () => {
        console.log("Press Rate App")
    }

    render(): React.Node {
        return (
            <Container style={styles.container}>
                <ScrollView>
                    <StatusBar translucent={false} barStyle="default" />
                    <Separator />
                    <ListItem
                        text="Terms and Conditions"
                        onPress={this.handleTermsandConditions}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <ListItem
                        text="Privacy Policy"
                        onPress={this.handlePrivatePolicy}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                    <ListItem
                        text="Credits"
                        onPress={this.handleCredits}
                        customIcon={
                            <Ionicons name={`${ICON_PREFIX}-arrow-forward`} color={ICON_COLOR} size={ICON_SIZE} />
                        }
                    />
                    <Separator />
                </ScrollView>
            </Container>
        )
    }
}

const styles = {
    container: {}
}
