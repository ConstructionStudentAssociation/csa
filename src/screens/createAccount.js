// @flow
import * as React from "react"
import { View, Text, StatusBar } from "react-native"
import { AuthContainer } from "../components/container"
import Logo from "../components/logo/logo"
import CreateAccountContainer from "../containers/createAccount/createAccountContainer"

import { StyleGuide } from "../config/theme"

const CreateAccount = () => {
    return (
        <AuthContainer style={styles.authContainer}>
            <StatusBar barStyle="light-content" translucent={false} />
            <View style={styles.container}>
                <Logo style={styles.logo} />

                <View style={styles.loginContainer}>
                    <CreateAccountContainer />
                </View>

                <View style={styles.foooterContainer}>
                    <Text style={styles.footerTxt}>By tapping up Sign Up & Accept, you agree to our sweet</Text>
                    <Text style={styles.footerTxt}>Terms of Service and very resonable Privacy Policy.</Text>
                </View>
            </View>
        </AuthContainer>
    )
}

export default CreateAccount

const styles = {
    logo: {
        resizeMode: "contain",
        top: 10
    },
    authContainer: {
        alignItems: "center"
    },
    altButtonContainer: {
        bottom: 40,
        alignItems: "center"
    },
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "space-around"
    },
    emailInput: {
        marginBottom: 45
    },
    footerTxt: {
        color: StyleGuide.palette.white,
        textAlign: "center"
    },
    primaryBtnContainer: {
        alignItems: "center"
    },
    primaryBtn: {
        top: 60
    },
    textField: {
        alignItems: "center"
    },
    loginContainer: {}
}
