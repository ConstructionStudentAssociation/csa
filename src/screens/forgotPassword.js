// @flow
import * as React from "react"
import { View } from "react-native"
import ForgotPasswordContainer from "../containers/forgotPasswoord/forgotPasswordContainer"

const ForgotPassword = () => {
    return (
        <View>
            <ForgotPasswordContainer />
        </View>
    )
}

export default ForgotPassword
