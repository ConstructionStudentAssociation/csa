// @flow
import * as React from "react"
import { View, Text, StatusBar } from "react-native"
import { AuthContainer } from "../components/container"
import Logo from "../components/logo/logo"
import LoginContainer from "../containers/login/loginContainer"

import { StyleGuide } from "../config/theme"


const Login = (props) => {
    return (
        <AuthContainer style={styles.authContainer}>
            <StatusBar
                barStyle="light-content"
                translucent={false}
            />
            <View style={styles.container}>
                <Logo style={styles.logo} />

                <View style={styles.loginContainer}>
                    <LoginContainer navigation={props.navigation} />
                </View>
                
                <View style={styles.footerContainer}>
                    <Text style={styles.footerTxt}>
                        Copyright 2017 Construction Student Association.
                    </Text>
                    <Text style={styles.footerTxt}>All rights reserved.</Text>
                </View>
            </View>
        </AuthContainer>
    )
}

export default Login

const styles = {
    logo: {
        resizeMode: "contain",
        top: 10
    },
    authContainer: {
        alignItems: "center"
    },
    altButtonContainer: {
        bottom: 40,
        alignItems: "center"
    },
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "space-around"
    },
    emailInput: {
        marginBottom: 45
    },
    footerTxt: {
        color: StyleGuide.palette.white,
        textAlign: "center"
    },
    primaryBtnContainer: {
        alignItems: "center"
    },
    primaryBtn: {
        top: 60
    },
    textField: {
        alignItems: "center"
    },
    loginContainer: {
        
    }
}
