// @flow
import * as React from "react"
import { View, StatusBar, FlatList } from "react-native"
import { ListItem, Separator } from "../components/list"

import listitems from "../data/list"

const SELECTION = "CAD"

export default class MemberList extends React.Component {
    handlePress = () => {
        console.log("row pressed")
    }

    render(): React.Node {
        return (
            <View style={{ flex: 1}}>
                <StatusBar barStyle="default" translucent={false} />
                <FlatList
                    data={listitems}
                    renderItem={({ item }) =>
                        (<ListItem
                            text={item}
                            selected={item === SELECTION}
                            onPress={this.handlePress}
                        />)
                    }
                    keyExtractor={item => item}
                    ItemSeparatorComponent={Separator}
                />
            </View>
        )

    }
}

