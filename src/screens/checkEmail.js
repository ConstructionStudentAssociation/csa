// @flow
import * as React from "react"
import { View, Text } from "react-native"
import { AuthContainer } from "../components/container"
import { StyleGuide } from "../config/theme"

const CheckEmail = () => {
    return (
        <AuthContainer style={styles.container}>
            <Text style={[styles.heading, styles.text]}>Check Your Email!</Text>
            <Text style={[styles.text, styles.subHeading]}>
                <Text>Instructions on how to reset your password</Text>
                <Text> are on their way!</Text>
            </Text>
        </AuthContainer>
    )
}

const styles = {
    container: {
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        color: StyleGuide.palette.white,
        width: 300,
        textAlign: "center",
        marginBottom: 30
    },
    heading: {
        ...StyleGuide.typography.title2
    },
    subHeading: {}
}

export default CheckEmail
