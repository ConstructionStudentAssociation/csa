// @flow
import * as React from "react"
import { View, Text, TouchableHighlight } from "react-native"
import Icon from "./icon"
import { StyleGuide } from "../../config/theme"


type ListItemProps = {
    text: string,
    onPress: () => void,
    selected: boolean,
    customIcon: () => void
};

const ListItem = (props: ListItemProps) => {
    return (
        <TouchableHighlight onPress={props.onPress} underlayColor={StyleGuide.palette.accent2}>
            <View style={styles.row}>
                <Text style={styles.text}>{props.text}</Text>
                {props.selected ? <Icon /> : <Icon />}
                {props.customIcon}
            </View>
        </TouchableHighlight>
    )
}

const styles = {
    row: {
        paddingHorizontal: 20,
        paddingVertical: 16,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: StyleGuide.palette.white
    },
    text: {
        fontSize: 16,
        color: StyleGuide.palette.black
    }
}

export default ListItem
