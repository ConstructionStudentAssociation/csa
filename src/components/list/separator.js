// @flow
import * as React from "react"
import { View, StyleSheet } from "react-native"
import { StyleGuide } from "../../config/theme"

const Separator = () => <View style={styles.separator} />

const styles = {
    separator: {
        marginLeft: 20,
        backgroundColor: StyleGuide.palette.separator,
        flex: 1,
        height: StyleSheet.hairlineWidth
    }
}
export default Separator
