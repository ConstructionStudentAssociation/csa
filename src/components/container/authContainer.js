// @flow
import * as React from "react"
import { StyleSheet, ImageBackground } from "react-native"

import background from "./image/background.png"

type AuthContainerProps = {
  children: () => void,
  style: () => void
};

export default class AuthContainer extends React.PureComponent<AuthContainerProps> {

    render(): React.Node {
        return (
            <ImageBackground source={background} style={[styles.container, this.props.style]}>{this.props.children}</ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
