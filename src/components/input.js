// @flow
import * as React from "react"
import { View, Text, TextInput, StyleSheet } from "react-native"

import { StyleGuide } from "../config/theme"

type InputProps = {
  titleText: string,
  placeholder: string,
  onChangeText: () => void,
  style: string
};

const Input = (props: InputProps) => {
    return (
        <View>
            <Text style={styles.title}>{props.titleText}</Text>
            <TextInput
                {...this.props}
                style={[styles.textInput, props.style]}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        color: StyleGuide.palette.accent2,
        fontSize: 24
    },
    textInput: {
        height: 32,
        borderBottomWidth: 1,
        borderBottomColor: StyleGuide.palette.accent2,
        color: StyleGuide.palette.white
    }
})
export default Input
