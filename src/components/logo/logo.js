// @flow
import * as React from "react"
import { View, Image, StyleSheet, Dimensions } from "react-native"
import { StyleGuide } from "../../config/theme/StyleGuide"

import csaLogo from "./images/csa_logo.png"

type LogoProps = {
    style: React.Node
};

const Logo = (props: LogoProps) => {
    return (
        <View style={styles.container}>
            <Image source={csaLogo} style={[styles.logo, props.style]} />
        </View>
    )
}

const { width } = Dimensions.get("window")

const styles = StyleSheet.create({
    container: {
        alignItems: "center"
    },
    logo: {
        width: width / 2
    }
})

export default Logo
