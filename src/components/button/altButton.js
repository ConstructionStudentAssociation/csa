// @flow
import * as React from "react"
import { Text, View, TouchableOpacity } from "react-native"
import { StyleGuide } from "../../config/theme"

type AltButtonProps = {
  title: string,
  subTitle: string,
  onPress: () => void
};

const AltButton = (props: AltButtonProps) => {
    return (
        <TouchableOpacity onPress={props.onPress}>
            <View style={styles.footer}>
                <Text style={styles.titleTxt}>{props.title}</Text>
                <Text style={styles.subTitle}>{props.subTitle}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = {
    footer: {
        flexDirection: "row"
    },
    titleTxt: {
        marginRight: 5,
        color: StyleGuide.palette.white
    },
    subTitle: {
        color: StyleGuide.palette.accent2
    }
}
export default AltButton
