// @flow
import * as React from "react"
import { TouchableHighlight, Dimensions, StyleSheet, Text } from "react-native"
import { StyleGuide } from "../../config/theme"

type DefaultButtonProps = {
  title: string,
  onPress: () => void
};

const DefaultButton = (props: DefaultButtonProps) => {
    return (
        <TouchableHighlight onPress={props.onPress} style={styles.button}>
            <Text style={styles.textColor}>{props.title}</Text>
        </TouchableHighlight>

    )
}

const buttonWidth = Dimensions.get("window").width
const styles = StyleSheet.create({
    button: {
        ...StyleGuide.styles.button,
        width: 278
    },
    textColor: {
        color: StyleGuide.palette.white
        // ...StyleGuide.typography.title3
    }
})

export default DefaultButton
