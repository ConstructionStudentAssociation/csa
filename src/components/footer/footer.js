// @flow
import * as React from "react"
import { View, Text } from "react-native"

type FooterProps = {
  footer: string
};

const Footer = (props: FooterProps) => {
    return (
        <View>
            <Text>{props.footer}</Text>
        </View>
    )
}

export default Footer
