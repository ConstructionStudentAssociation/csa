// @flow
import { StackNavigator } from "react-navigation"

import Login from "../screens/login"
import CreateAccount from "../screens/createAccount"

const SignedOut = StackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            header: () => null
        }
    },
    CreateAccount: {
        screen: CreateAccount,
        navigationOptions: {
            header: () => null
        }
    }
})

export default SignedOut
