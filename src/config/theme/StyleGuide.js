// @flow
import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"

export type StyleProps = {
    style?: StyleObj
};

export type Typography = {
    fontFamily: string,
    fontSize: number,
    lineHeight: number
};

export type Typographies = {
    body: Typography,
    callout: Typography,
    caption: Typography,
    footnote: Typography,
    headline: Typography,
    subhead: Typography,
    title1: Typography,
    title2: Typography,
    title3: Typography
};

export type Spacing = {
    tiny: number,
    small: number,
    base: number,
    large: number,
    xLarge: number
};

export type Palette = {
    black: string,
    darkGray: string,
    gray: string,
    lightGray: string,
    white: string
};

export type Shadow = {
    shadowColor: string,
    shadowOffset: { width: number, height: number },
    shadowOpacity: number,
    shadowRadius: number
};

export type Styles = {
    barHeight: {
        height: number
    },
    shadow: Shadow,
    borderRadius: {
        borderRadius: number
    },
    separator: {
        borderBottomWidth: number,
        borderColor: string
    },
    button: {
        height: number,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        padding: number,
        marginBottom: number,
        borderRadius: number
    },
    buttonIcon: {
        marginRight: number
    }
};

export type StyleGuide = {
    palette: Palette,
    typography: Typographies,
    spacing: Spacing,
    styles: Styles
};

const darkGray = "#999999"
const gray = "#CCCCCC"
const tiny = 8
const small = 16
const large = 22
const borderRadius = large
const shadow = {
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.18,
    shadowRadius: 2,
    elevation: 1
}

const styleGuide: StyleGuide = {
    palette: {
        black: "#222",
        base: "#501214",
        accent1: "#800020",
        accent2: "#8D734A",
        accent3: "#2A010B",
        gray: "#5B5B5B",
        gray2: "#D8D8D8",
        gray3: "#EEE",
        success: "#AEF78E",
        error: "#FF3232",
        warning: "#FFD671",
        white: "#FEFEFE",
        separator: "#E8E8E8"
    },
    typography: {
        body: {
            fontSize: 17,
            lineHeight: 20,
            fontFamily: "SFProText-Regular"
        },
        callout: {
            fontSize: 16,
            lineHeight: 24,
            fontFamily: "SFProText-Regular"
        },
        caption: {
            fontSize: 11,
            lineHeight: 13,
            fontFamily: "SFProText-Regular"
        },
        footnote: {
            fontSize: 13,
            lineHeight: 18,
            fontFamily: "SFProText-Regular",
            color: darkGray
        },
        headline: {
            fontSize: 17,
            lineHeight: 22,
            fontFamily: "SFProText-Semibold"
        },
        subhead: {
            fontSize: 15,
            lineHeight: 20,
            fontFamily: "SFProText-Regular"
        },
        title1: {
            fontSize: 34,
            lineHeight: 41,
            fontFamily: "SFProText-Bold"
        },
        title2Bold: {
            fontSize: 28,
            lineHeight: 34,
            fontFamily: "SFProText-Bold"
        },
        title2: {
            fontSize: 28,
            lineHeight: 34,
            fontFamily: "SFProText-Regular"
        },
        title3Bold: {
            fontSize: 22,
            lineHeight: 26,
            fontFamily: "SFProText-Bold"
        },
        title3: {
            fontSize: 22,
            lineHeight: 26,
            fontFamily: "SFProText-Regular"
        }
    },
    spacing: {
        tiny,
        small,
        base: 24,
        large: 48,
        xLarge: 64
    },
    styles: {
        barHeight: {
            height: 45
        },
        shadow,
        borderRadius: {
            borderRadius
        },
        separator: {
            borderBottomWidth: 1,
            borderColor: "#ebebeb"
        },
        button: {
            height: 45,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            marginBottom: small,
            padding: tiny,
            backgroundColor: "#222",
            borderRadius
        },
        buttonIcon: {
            marginRight: tiny
        }
    }
}

export default styleGuide
