// @flow
import * as React from "react"
import { View } from "react-native"
import Input from "../../components/input"
import { DefaultButton } from "../../components/button"
import { StyleGuide } from "../../config/theme"

export default class ForgotPasswordForm extends React.Component {
    render(): React.Node {
        const { handleSubmit } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.textField}>
                    <View style={styles.emailInput}>
                        <Input
                            titleText="Email"
                            label="student@txstate.edu"
                            keyboardType="email-address"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            style={styles.field}
                        />
                    </View>
                    <View style={styles.primaryBtnContainer}>
                        <DefaultButton
                            title="Request password reset"
                            onSubmit={handleSubmit}
                            style={styles.primaryBtn}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = {
    emailInput: {
        marginBottom: 45
    },
    textField: {
        alignItems: "center",
        bottom: 44
    },
    primaryBtnContainer: {
        alignItems: "center"
    },
    field: {
        width: 262
    }
}
