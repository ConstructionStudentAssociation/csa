// @flow
import * as React from "react"
import { View, Text } from "react-native"
import ForgotPasswordForm from "./forgotPasswordForm"

const ForgotPasswordContainer = () => {
    return (
        <View>
            <Text>Forgot Your Password?</Text>
            <Text> We'll send you instructions to reset your password.</Text>
            <ForgotPasswordForm />
        </View>
    )
}

export default ForgotPasswordContainer
