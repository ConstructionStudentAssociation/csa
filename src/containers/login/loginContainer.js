// @flow
import * as React from "react"
import { View } from "react-native"
import { DefaultButton, AltButton } from "../../components/button"
import LoginForm from "./loginForm"

type LoginContainerProps = {
    navigation: () => void,
    navigate: () => void
};

class LoginContainer extends React.Component<LoginContainerProps> {
    handleSignInPress = () => {
        this.props.navigation.navigate("CreateAccount")
    }
    render(): React.Node {
        return (
            <View style={styles.container}>
                <LoginForm onSubmit={this.onLoginPressed} />

                <View style={styles.primaryBtnContainer}>
                    <DefaultButton title="Login" onPress={this.onLoginPressed} style={styles.primaryBtn} />
                </View>
                <View style={styles.altButtonContainer}>
                    <AltButton title="Don't have an account?" subTitle="Sign Up" onPress={this.handleSignInPress} />
                </View>
            </View>
        )
    }
}

const styles = {
    altButtonContainer: {
        bottom: -30,
        alignItems: "center"
    },
    primaryBtnContainer: {
        alignItems: "center"
    }
}

export default LoginContainer
