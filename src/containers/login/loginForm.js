// @flow
import * as React from "react"
import { View } from "react-native"
import Input from "../../components/input"
import { DefaultButton } from "../../components/button"
import { StyleGuide } from "../../config/theme"



export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render(): React.Node {
        const { handleSubmit } = this.props
        console.log(handleSubmit)
        return (
            <View style={styles.container}>
                <View style={styles.textField}>
                    <View style={styles.emailInput}>
                        <Input
                            titleText="Email"
                            label="student@txstate.edu"
                            keyboardType="email-address"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            type="text"
                            style={styles.field}
                        />
                    </View>
                    <Input
                        titleText="Password"
                        label="student@txstate.edu"
                        secureTextEntry
                        type="text"
                        enableReturnKeyAutomatically
                        style={styles.field}
                    />
                </View>
            </View>
        )
    }
}

const styles = {
    emailInput: {
        marginBottom: 45
    },
    textField: {
        alignItems: "center",
        bottom: 44
    },
    field: {
        width: 262
    }
}
