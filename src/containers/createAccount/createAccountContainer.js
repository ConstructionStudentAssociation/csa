// @flow
import * as React from "react"
import { View, Text } from "react-native"
import CreateAccountForm from "./createAccountForm"
import { AltButton } from "../../components/button"

export default class CreateAccountContaier extends React.PureComponent {
    render(): React.Node {
        return (
            <View>
                <CreateAccountForm />

                <View style={styles.altButtonContainer}>
                    <AltButton title="Already have an account?" subTitle="Sign In" onPress={this.onNeedAccountPressed} />
                </View>
            </View>
        )
    }
}

const styles = {
    altButtonContainer: {
        bottom: -30,
        alignItems: "center"
    }
}
