// @flow
import * as React from "react"
import { View } from "react-native"
import Input from "../../components/input"
import { DefaultButton } from "../../components/button"
import { StyleGuide } from "../../config/theme"

export default class CreateAccountForm extends React.Component {
    render(): React.Node {
        const { handleSubmit } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.textField}>
                    <View style={styles.nameContainer}>
                        <Input
                            titleText="First Name"
                            label="student@txstate.edu"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            style={styles.nameField}
                        />
                        <Input
                            titleText="Last Name"
                            label="student@txstate.edu"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            style={styles.nameField}
                        />
                    </View>
                    <View style={styles.memberTypeContainer}>
                        <Input
                            titleText="Member Type"
                            label="Select a Membership Type"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            style={styles.field}
                        />
                    </View>
                    <View style={styles.phoneContainer}>
                        <Input
                            titleText="Phone Number"
                            label="123-456-7890"
                            keyboardType="numeric"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            style={styles.field}
                        />
                    </View>
                    <View style={styles.emailInput}>
                        <Input
                            titleText="Email"
                            label="student@txstate.edu"
                            keyboardType="email-address"
                            enableReturnKeyAutomatically
                            autoCorrect={false}
                            style={styles.field}
                        />
                    </View>
                    <Input
                        titleText="Password"
                        label="student@txstate.edu"
                        secureTextEntry
                        enableReturnKeyAutomatically
                        style={styles.field}
                    />
                </View>
                <View style={styles.primaryBtnContainer}>
                    <DefaultButton title="Sign up & Accept" onSubmit={handleSubmit} style={styles.primaryBtn} />
                </View>
            </View>
        )
    }
}

const styles = {
    emailInput: {
        marginBottom: 45
    },
    textField: {
        alignItems: "center",
        bottom: 44
    },
    primaryBtnContainer: {
        alignItems: "center"
    },
    field: {
        width: 262
    },
    nameField: {
        width: 129
    },
    nameContainer: {
        flexDirection: "row",
        margin: 2
    }
}
