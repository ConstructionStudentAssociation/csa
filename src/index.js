// @flow
import * as React from "react"
import { Font, AppLoading } from "expo"

import Navigator from "./config/appRouteConfig"

type State = {
  fontLoaded: boolean
};

export default class App extends React.Component<null, State> {
    state = {
        fontLoaded: false
    }

    async componentDidMount() {
        await Font.loadAsync({
            "SFProText-Bold": require("../fonts/SF-Pro-Text-Bold.otf"),
            "SFProText-Semibold": require("../fonts/SF-Pro-Text-Semibold.otf"),
            "SFProText-Regular": require("../fonts/SF-Pro-Text-Regular.otf")
        }).then(() => {
            this.setState({
                fontLoaded: true
            })
        })
    }


    render(): React.node {
        return !this.state.fontLoaded ? <AppLoading /> : <Navigator />


    }
}
